﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;


namespace WRTOZ1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Znajdź długość najdłuższego prefiksu, który jest jednocześnie sufiksem podanego łańcucha, 
            //ale nie jest to cały łańcuch. Na przykład dla łańcucha cabaca odpowiedzią jest długość 2, 
            //bo ca spełnia podane warunki. Dla aaaa oraz abc odpowiedziami są, odpowiednio, długości 3 oraz 0. 
            //Zakładamy, że wejściowy łańcuch składa się z liter a − z, A − Z, a jego długość może wynosić 10^6. 
            //Limit czasowy = 10 sekund.

            //generuje łancuch o długości 10^6 skłądający się z liter a-z, A-Z
            long length = 1000000;

            StringBuilder str_build = new StringBuilder();
            Random random = new Random();

            char letter;

            Console.WriteLine("Buduje lancuch");

            for (int i = 0; i < length; i++)
            {
                double flt = random.NextDouble();
                double size = random.NextDouble();
                if (size >= 0.5)
                {
                    //wielkie litery
                    int shift = Convert.ToInt32(Math.Floor(25 * flt));
                    letter = Convert.ToChar(shift + 65);
                    str_build.Append(letter);
                }
                else
                {
                    //male litery
                    int shift = Convert.ToInt32(Math.Floor(25 * flt));
                    letter = Convert.ToChar(shift + 97);
                    str_build.Append(letter);
                }
            }

            System.Console.WriteLine(str_build.ToString());
            string chain = str_build.ToString();

            //Console.WriteLine("Buduje lancuch");

            //test case lancuch aaa odkomentowac

            //StringBuilder str_build2 = new StringBuilder();

            //string chain = null;
            //for(long d=0; d < length; d++)
            //{
            //    str_build2.Append("a");
            //}

            //chain = str_build2.ToString();

            Console.WriteLine("Buduje lancuch: Koniec");

            //Start wyszukiwania łancuchu znaków Algorytm MP
            Stopwatch stopWatch = new Stopwatch();
            DateTime dateTime = DateTime.Now;
            Console.WriteLine(dateTime);
            stopWatch.Start();

            int[] PI = new int[length + 1];
            int b = -1;

            PI[0] = b;

            for (int j=1; j<=length; j++)
            {
                while((b >-1) && (chain[b] != chain[j - 1]))
                {
                    b = PI[b];
                }
                PI[j] = b++;
               
            }
           
            dateTime = DateTime.Now;
            stopWatch.Stop();
            long duration = stopWatch.Elapsed.Milliseconds;

            Console.WriteLine("Najdłuższy poszukiwany prefix/surfix wynosi: "+ b );
            Console.WriteLine("Czas potrzebny do znalezienia rozwiazania: " + duration + " milisekund");
            Console.WriteLine(dateTime);

            Console.ReadKey();

        }
    }
}
